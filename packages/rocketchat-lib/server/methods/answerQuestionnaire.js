import { Meteor } from 'meteor/meteor';
import { Match } from 'meteor/check';

Meteor.methods({
	answerQuestionnaire(data = {}) {
		// Validate user and room
		const answers = Meteor.user().answers;

        if (_.isEmpty(answers) == false) {
            throw new Meteor.Error('error-already-answered', 'You are allready answered this.', {
				method: 'answerQuestionnaire',
			});
        }

		if (!Match.test(data.answers.answer1, String)) {
			throw new Meteor.Error('error-invalid-answer-1', 'Invalid Answer', {
				method: 'answerQuestionnaire',
			});
        }

		if (!Match.test(data.answers.answer2, String)) {
			throw new Meteor.Error('error-invalid-answer-1', 'Invalid Answer', {
				method: 'answerQuestionnaire',
			});
        }

		if (!Match.test(data.answers.answer3, String)) {
			throw new Meteor.Error('error-invalid-answer-1', 'Invalid Answer', {
				method: 'answerQuestionnaire',
			});
		}

        RocketChat.models.Users.setAnswers({"username": "test"}, data.answers);

		return true;
	},
});
