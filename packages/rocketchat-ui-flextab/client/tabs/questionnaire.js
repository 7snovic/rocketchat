import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { Tracker } from 'meteor/tracker';
import { Session } from 'meteor/session';
import { Template } from 'meteor/templating';
import { RocketChat, handleError } from 'meteor/rocketchat:lib';
import { t, ChatRoom, RoomManager, popover, isRtl } from 'meteor/rocketchat:ui';
import { WebRTC } from 'meteor/rocketchat:webrtc';
import _ from 'underscore';
import { Mongo } from 'meteor/mongo';

Template.questionnaire.helpers({
    answers() {
        var answers = Template.instance().answers();

        console.log('helpers', answers);

        if (_.isEmpty(answers)) {
            return {};
        }

        return answers;
    }
});

Template.questionnaire.events({
    'submit .submit-answers'(e, i) {
        const target = e.target;

        e.preventDefault();

        Meteor.call('answerQuestionnaire', {
            answers: {
                "answer1": target.answer1.value,
                "answer2": target.answer2.value,
                "answer3": target.answer3.value
            },
        }, function(err) {
            console.log(err);
        });
    }
});

Template.questionnaire.onCreated(function () {

    this.answers = function () {

        const a = Meteor.users.find({"username": "test"}).fetch();

        console.log(a);

        return a.answers;
    };

    console.log('oncreated', this.answers());

});
